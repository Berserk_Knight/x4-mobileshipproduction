﻿-- param == { 0, 0 }

-- modes: 

-- ffi setup
local ffi = require("ffi")
local C = ffi.C
ffi.cdef[[
	typedef uint64_t BuildTaskID;
	typedef uint64_t UniverseID;
	typedef struct {
		const char* macro;
		const char* ware;
		uint32_t amount;
		uint32_t capacity;
	} AmmoData;
	typedef struct {
		BuildTaskID id;
		UniverseID buildingcontainer;
		UniverseID component;
		const char* macro;
		const char* factionid;
		UniverseID buildercomponent;
		int64_t price;
		bool ismissingresources;
		uint32_t queueposition;
	} BuildTaskInfo;
	typedef struct {
		UniverseID softtargetID;
		const char* softtargetConnectionName;
	} SofttargetDetails;
	typedef struct {
		const char* file;
		const char* icon;
		bool ispersonal;
	} UILogo;
	typedef struct {
		const char* path;
		const char* group;
	} UpgradeGroup;
	typedef struct {
		UniverseID currentcomponent;
		const char* currentmacro;
		const char* slotsize;
		uint32_t count;
		uint32_t operational;
		uint32_t total;
	} UpgradeGroupInfo;
	bool CanActivateSeta(bool checkcontext);
	bool CanCancelConstruction(UniverseID containerid, BuildTaskID id);
	bool CanPlayerStandUp(void);
	bool CanStartTravelMode(UniverseID objectid);
	uint32_t GetAllLaserTowers(AmmoData* result, uint32_t resultlen, UniverseID defensibleid);
	uint32_t GetAllMines(AmmoData* result, uint32_t resultlen, UniverseID defensibleid);
	uint32_t GetAllNavBeacons(AmmoData* result, uint32_t resultlen, UniverseID defensibleid);
	uint32_t GetAllResourceProbes(AmmoData* result, uint32_t resultlen, UniverseID defensibleid);
	uint32_t GetAllSatellites(AmmoData* result, uint32_t resultlen, UniverseID defensibleid);
	uint32_t GetBuildTasks(BuildTaskInfo* result, uint32_t resultlen, UniverseID containerid, bool isinprogress, bool includeupgrade);
	const char* GetComponentName(UniverseID componentid);
	UniverseID GetContextByClass(UniverseID componentid, const char* classname, bool includeself);
	UILogo GetCurrentPlayerLogo(void);
	UniverseID GetEnvironmentObject();
	uint32_t GetNumAllLaserTowers(UniverseID defensibleid);
	uint32_t GetNumAllMines(UniverseID defensibleid);
	uint32_t GetNumAllNavBeacons(UniverseID defensibleid);
	uint32_t GetNumAllResourceProbes(UniverseID defensibleid);
	uint32_t GetNumAllSatellites(UniverseID defensibleid);
	uint32_t GetNumBuildTasks(UniverseID containerid, bool isinprogress, bool includeupgrade);
	uint32_t GetNumUpgradeGroups(UniverseID destructibleid, const char* macroname);
	const char* GetObjectIDCode(UniverseID objectid);
	UniverseID GetPlayerID(void);
	UniverseID GetPlayerShipID(void);
	SofttargetDetails GetSofttarget(void);
	float GetTextHeight(const char*const text, const char*const fontname, const float fontsize, const float wordwrapwidth);
	UniverseID GetTopLevelContainer(UniverseID componentid);
	const char* GetTurretGroupMode(UniverseID defensibleid, const char* path, const char* group);
	bool GetUp(void);
	UpgradeGroupInfo GetUpgradeGroupInfo(UniverseID destructibleid, const char* macroname, const char* path, const char* group, const char* upgradetypename);
	uint32_t GetUpgradeGroups(UpgradeGroup* result, uint32_t resultlen, UniverseID destructibleid, const char* macroname);
	const char* GetWeaponMode(UniverseID weaponid);
	bool HasShipFlightAssist(UniverseID shipid);
	bool IsComponentClass(UniverseID componentid, const char* classname);
	void LaunchLaserTower(UniverseID defensibleid, const char* lasertowermacroname);
	void LaunchMine(UniverseID defensibleid, const char* minemacroname);
	void LaunchNavBeacon(UniverseID defensibleid, const char* navbeaconmacroname);
	void LaunchResourceProbe(UniverseID defensibleid, const char* resourceprobemacroname);
	void LaunchSatellite(UniverseID defensibleid, const char* satellitemacroname);
	bool QuickDock();
	bool RequestDockAt(UniverseID containerid, bool checkonly);
	void SetAllTurretModes(UniverseID defensibleid, const char* mode);
	void SetTurretGroupMode(UniverseID defensibleid, const char* path, const char* group, const char* mode);
	void SetWeaponMode(UniverseID weaponid, const char* mode);
	void StartPlayerActivity(const char* activityid);
	void StopPlayerActivity(const char* activityid);
	bool ToggleAutoPilot(bool checkonly);
	void ToggleFlightAssist();
	int32_t UndockPlayerShip(bool checkonly);
]]

-- menu variable - used by Helper and used for dynamic variables (e.g. inventory content, etc.)
local menu = {
	name = "DockedMenu"
}

-- config variable - put all static setup here
local config = {
	modes = {
		[1] = { id = "travel",			name = ReadText(1002, 1158),	stoptext = ReadText(1002, 1159),	action = 303 },
		[2] = { id = "scan",			name = ReadText(1002, 1156),	stoptext = ReadText(1002, 1157),	action = 304 },
		[3] = { id = "scan_longrange",	name = ReadText(1002, 1155),	stoptext = ReadText(1002, 1160),	action = 305 },
		[4] = { id = "seta",			name = ReadText(1001, 1132),	stoptext = ReadText(1001, 8606),	action = 225 },
	},
	consumables = {
		{ id = "satellite",		type = "civilian",	getnum = C.GetNumAllSatellites,		getdata = C.GetAllSatellites,		callback = C.LaunchSatellite },
		{ id = "navbeacon",		type = "civilian",	getnum = C.GetNumAllNavBeacons,		getdata = C.GetAllNavBeacons,		callback = C.LaunchNavBeacon },
		{ id = "resourceprobe",	type = "civilian",	getnum = C.GetNumAllResourceProbes,	getdata = C.GetAllResourceProbes,	callback = C.LaunchResourceProbe },
		{ id = "lasertower",	type = "military",	getnum = C.GetNumAllLaserTowers,	getdata = C.GetAllLaserTowers,		callback = C.LaunchLaserTower },
		{ id = "mine",			type = "military",	getnum = C.GetNumAllMines,			getdata = C.GetAllMines,			callback = C.LaunchMine },
	},
	inactiveButtonProperties = { bgColor = Helper.defaultUnselectableButtonBackgroundColor, highlightColor = Helper.defaultUnselectableButtonHighlightColor },
	activeButtonTextProperties = { halign = "center" },
	inactiveButtonTextProperties = { halign = "center", color = Helper.color.grey },
}

-- init menu and register with Helper
local function init()
	--print("Initializing")
	Menus = Menus or { }
	table.insert(Menus, menu)
	if Helper then
		Helper.registerMenu(menu)
	end

	menu.init = true
	registerForEvent("gameplanchange", getElement("Scene.UIContract"), menu.onGamePlanChange)
end

function menu.onGamePlanChange(_, mode)
	if menu.init then
		if (mode == "cockpit") or (mode == "external") then
			local occupiedship = ConvertStringTo64Bit(tostring(C.GetPlayerOccupiedShipID()))
			if (occupiedship ~= 0) and GetComponentData(occupiedship, "isdocked") then
				OpenMenu("DockedMenu", { 0, 0 }, nil)
			end
			menu.init = nil
		elseif (mode == "firstperson") or (mode == "externalfirstperson") then
			menu.init = nil
		end
	end
end

-- cleanup variables in menu, no need for the menu variable to keep all the data while the menu is not active
function menu.cleanup()
	unregisterForEvent("inputModeChanged", getElement("Scene.UIContract"), menu.onInputModeChanged)
	menu.topLevelOffsetY = nil

	--print("Cleaning Up")
	UnregisterEvent("playerUndock", menu.close)
	UnregisterEvent("playerGetUp", menu.close)
	menu.currentcontainer = nil
	--menu.topcontainer = nil
	menu.currentplayership = nil
	menu.playerInfo = {}
	menu.mode = nil
	menu.buildInProgress = nil
	menu.buildToCancel = nil
	menu.turrets = {}
	menu.turretgroups = {}

	menu.frame = nil
	menu.table_toplevel = nil
	menu.table_topleft = nil
	menu.table_header = nil
end

function menu.onShowMenu()
	Helper.setTabScrollCallback(menu, menu.onTabScroll)
	registerForEvent("inputModeChanged", getElement("Scene.UIContract"), menu.onInputModeChanged)

	-- close the menu if player either undocks or gets up while the menu is open.
	RegisterEvent("playerUndock", menu.close)
	RegisterEvent("playerGetUp", menu.close)

	--print("Showing Menu")
	-- NB: we have just docked so current player ship is the relevant ship for the entirety of this menu's duration. update here if this changes.
	menu.currentplayership = ConvertStringTo64Bit(tostring(C.GetPlayerOccupiedShipID()))
	if menu.currentplayership ~= 0 then
		if GetComponentData(menu.currentplayership, "isdocked") then
			menu.mode = "docked"
			menu.currentcontainer = ConvertStringTo64Bit(tostring(C.GetContextByClass(menu.currentplayership, "container", false)))
		else
			menu.mode = "cockpit"
			menu.currentcontainer = menu.currentplayership
		end
	else
		menu.currentcontainer = ConvertStringTo64Bit(tostring(C.GetContextByClass(C.GetPlayerID(), "container", false)))
		if C.IsComponentClass(menu.currentcontainer, "ship") and (not GetComponentData(menu.currentcontainer, "isdocked")) then
			menu.mode = "cockpit"
		else
			menu.mode = "docked"
		end
	end
	--menu.topcontainer = ConvertStringTo64Bit(tostring(C.GetTopLevelContainer(menu.currentplayership)))
	--print("current player ship: " .. ffi.string(C.GetComponentName(menu.currentplayership)) .. ", currentcontainer: " .. ffi.string(C.GetComponentName(menu.currentcontainer)) .. ", topcontainer: " .. ffi.string(C.GetComponentName(menu.topcontainer)))

	menu.selectedRows = {}
	menu.selectedCols = {}
	-- init selection
	menu.selectedRows.header = 5
	menu.selectedCols.header = 2

	-- add content
	menu.display()
end

function menu.display()
	Helper.removeAllWidgetScripts(menu)

	local width = Helper.viewWidth
	local height = Helper.viewHeight
	local xoffset = 0
	local yoffset = 0

	menu.frame = Helper.createFrameHandle(menu, { width = width, x = xoffset, y = yoffset, backgroundID = "solid", backgroundColor = Helper.color.semitransparent, standardButtons = ((menu.mode == "docked") and (menu.currentplayership ~= 0)) and {} or { close = true, back = true } })

	menu.createTopLevel(menu.frame)

	local table_topleft, table_header, table_button, row

	local isdocked = (menu.currentplayership ~= 0) and GetComponentData(menu.currentplayership, "isdocked")
	local ownericon, owner, shiptrader, isdock, canbuildships = GetComponentData(menu.currentcontainer, "ownericon", "owner", "shiptrader", "isdock", "canbuildships")
	-- TODO: re-add ware exchange here
	local cantrade = isdock and (C.IsComponentClass(menu.currentcontainer, "station") or (C.IsComponentClass(menu.currentcontainer, "ship") and owner == "player"))
	--NB: equipment docks currently do not have ship traders
	local dockedplayerships = {}
	Helper.ffiVLA(dockedplayerships, "UniverseID", C.GetNumDockedShips, C.GetDockedShips, menu.currentcontainer, "player")
	local canequip = false
	for _, ship in ipairs(dockedplayerships) do
		if C.CanContainerEquipShip(menu.currentcontainer, ship) then
			canequip = true
		end
	end
	local canmodifyship = ((shiptrader ~= nil and canequip) or (owner == "player" and canequip)) and isdock
	local canbuyship = ((shiptrader ~= nil and canbuildships) or (owner == "player" and canbuildships)) and isdock
	--print("cantrade: " .. tostring(cantrade) .. ", canbuyship: " .. tostring(canbuyship) .. ", canmodifyship: " .. tostring(canmodifyship))

	width = (width / 3) - Helper.borderSize

	menu.playerInfo = {
		width = 0.3 * Helper.viewWidth,
		height = 3 * (Helper.scaleY(Helper.standardTextHeight) + Helper.borderSize),
		offsetX = 25,
		offsetY = 25,
		fontsize = Helper.scaleFont(Helper.standardFont, Helper.standardFontSize),
	}

	-- set up a new table
	table_topleft = menu.frame:addTable(1, { tabOrder = 0, width = menu.playerInfo.width, height = menu.playerInfo.height, x = menu.playerInfo.offsetX, y = menu.playerInfo.offsetY, scaling = false })

	row = table_topleft:addRow(false, { fixed = true, bgColor = Helper.color.transparent60 })
	local icon = row[1]:createIcon(function () local logo = C.GetCurrentPlayerLogo(); return ffi.string(logo.icon) end, { width = menu.playerInfo.height, height = menu.playerInfo.height })

	local fontsize = Helper.scaleFont(Helper.standardFont, Helper.standardFontSize)
	local textheight = math.ceil(C.GetTextHeight(Helper.playerInfoTextLeft(), Helper.standardFont, menu.playerInfo.fontsize, menu.playerInfo.width - menu.playerInfo.height - Helper.borderSize))
	icon:setText(Helper.playerInfoTextLeft,	{ fontsize = menu.playerInfo.fontsize, halign = "left",  x = menu.playerInfo.height + Helper.borderSize, y = (menu.playerInfo.height - textheight) / 2 })
	icon:setText2(Helper.playerInfoTextRight,	{ fontsize = menu.playerInfo.fontsize, halign = "right", x = Helper.borderSize,          y = (menu.playerInfo.height - textheight) / 2 })

	local xoffset = (Helper.viewWidth - width) / 2
	local yoffset = 25

	table_header = menu.frame:addTable(11, { tabOrder = 1, width = width, x = xoffset, y = menu.topLevelOffsetY + Helper.borderSize + yoffset })
	table_header:setColWidth(1, math.floor((width - 2 * Helper.borderSize) / 3), false)
	table_header:setColWidth(3, Helper.standardTextHeight)
	table_header:setColWidth(4, Helper.standardTextHeight)
	table_header:setColWidth(5, Helper.standardTextHeight)
	table_header:setColWidth(6, Helper.standardTextHeight)
	table_header:setColWidth(8, Helper.standardTextHeight)
	table_header:setColWidth(9, Helper.standardTextHeight)
	table_header:setColWidth(10, Helper.standardTextHeight)
	table_header:setColWidth(11, Helper.standardTextHeight)
	table_header:setDefaultColSpan(1, 1)
	table_header:setDefaultColSpan(2, 5)
	table_header:setDefaultColSpan(7, 5)

	local row = table_header:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
	local color = Helper.color.white
	if GetComponentData(menu.currentcontainer, "isplayerowned") then
		if menu.currentcontainer == C.GetPlayerObjectID() then
			color = Helper.color.playergreen
		else
			color = Helper.color.green
		end
	end
	row[1]:setColSpan(11):createText(menu.currentcontainer and ffi.string(C.GetComponentName(menu.currentcontainer)) or "", Helper.headerRowCenteredProperties)
	row[1].properties.color = color

	height = Helper.scaleY(Helper.standardTextHeight)

	local row = table_header:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
	if menu.mode == "cockpit" then
		row[2]:createText(ffi.string(C.GetObjectIDCode(menu.currentcontainer)), { halign = "center", color = color })
	else
		row[1]:createIcon(ownericon, { width = height, height = height, x = row[1]:getWidth() - height, scaling = false })
		row[2]:createText(function() return GetComponentData(menu.currentcontainer, "ownername") end, { halign = "center" })
		row[7]:createText(function() return "[" .. GetUIRelation(GetComponentData(menu.currentcontainer, "owner")) .. "]" end, { halign = "left" })
	end

	local row = table_header:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
	row[1]:createText("", { height = yoffset })

	if menu.mode == "cockpit" then
		local row = table_header:addRow("buttonRow1", { bgColor = Helper.color.transparent, fixed = true })
		row[1]:createButton(config.inactiveButtonProperties):setText("", config.inactiveButtonTextProperties)	-- dummy
		local active = (menu.currentplayership ~= 0) and C.CanPlayerStandUp()
		row[2]:createButton(active and { mouseOverText = GetLocalizedKeyName("action", 277) } or config.inactiveButtonProperties):setText(ReadText(1002, 20014), active and config.activeButtonTextProperties or config.inactiveButtonTextProperties)	-- "Get Up"
		if active then
			row[2].handlers.onClick = menu.buttonGetUp
		end
		row[7]:createButton({ mouseOverText = GetLocalizedKeyName("action", 316) }):setText(ReadText(1001, 8602), { halign = "center" })	-- "Ship Information"
		row[7].handlers.onClick = menu.buttonShipInfo

		local row = table_header:addRow("buttonRow3", { bgColor = Helper.color.transparent, fixed = true })
		local currentactivity = GetPlayerActivity()
		if currentactivity ~= "none" then
			local text = ""
			for _, entry in ipairs(config.modes) do
				if entry.id == currentactivity then
					text = entry.stoptext
					break
				end
			end
			local active = menu.currentplayership ~= 0
			row[2]:createButton(active and {} or config.inactiveButtonProperties):setText(text, active and config.activeButtonTextProperties or config.inactiveButtonTextProperties)	-- "Stop Mode"
			if active then
				row[2].handlers.onClick = menu.buttonStopMode
				row[2].properties.uiTriggerID = "stopmode"
			end
		else
			local active = menu.currentplayership ~= 0
			local modes = {}
			if active then
				for _, entry in ipairs(config.modes) do
					local active = true
					local visible = true
					if entry.id == "travel" then
						active = (menu.currentplayership ~= 0) and C.CanStartTravelMode(menu.currentplayership)
					elseif entry.id == "seta" then
						visible = C.CanActivateSeta(false)
					end
					local mouseovertext = GetLocalizedKeyName("action", entry.action)
					if visible then
						table.insert(modes, { id = entry.id, text = entry.name, icon = "", displayremoveoption = false, active = active, mouseovertext = mouseovertext })
					end
				end
			end
			row[2]:createDropDown(modes, {
				height = Helper.standardButtonHeight,
				startOption = "",
				textOverride = ReadText(1002, 1001),
				bgColor = active and Helper.defaultButtonBackgroundColor or Helper.defaultUnselectableButtonBackgroundColor,
				highlightColor = active and Helper.defaultButtonHighlightColor or Helper.defaultUnselectableButtonHighlightColor
			}):setTextProperties(active and config.activeButtonTextProperties or config.inactiveButtonTextProperties)	-- Modes
			if active then
				row[2].handlers.onDropDownConfirmed = menu.dropdownMode
				row[2].properties.uiTriggerID = "startmode"
			end
		end
		local civilian, military = {}, {}
		if menu.currentplayership ~= 0 then
			for _, consumabledata in ipairs(config.consumables) do
				local numconsumable = consumabledata.getnum(menu.currentplayership)
				if numconsumable > 0 then
					local consumables = ffi.new("AmmoData[?]", numconsumable)
					numconsumable = consumabledata.getdata(consumables, numconsumable, menu.currentplayership)
					for j = 0, numconsumable - 1 do
						if consumables[j].amount > 0 then
							local macro = ffi.string(consumables[j].macro)
							if consumabledata.type == "civilian" then
								table.insert(civilian, { id = consumabledata.id .. ":" .. macro, text = GetMacroData(macro, "name"), text2 = "(" .. consumables[j].amount .. ")", icon = "", displayremoveoption = false })
							else
								table.insert(military, { id = consumabledata.id .. ":" .. macro, text = GetMacroData(macro, "name"), text2 = "(" .. consumables[j].amount .. ")", icon = "", displayremoveoption = false })
							end
						end
					end
				end
			end
		end
		local active = #civilian > 0
		row[1]:createDropDown(civilian, {
			height = Helper.standardButtonHeight,
			startOption = "",
			textOverride = ReadText(1001, 8607),
			text2Override = " ",
			bgColor = active and Helper.defaultButtonBackgroundColor or Helper.defaultUnselectableButtonBackgroundColor,
			highlightColor = active and Helper.defaultButtonHighlightColor or Helper.defaultUnselectableButtonHighlightColor
		}):setTextProperties(active and config.activeButtonTextProperties or config.inactiveButtonTextProperties):setText2Properties(active and config.activeButtonTextProperties or config.inactiveButtonTextProperties)	-- Deploy Civilian
		row[1].properties.text2.halign = "right"
		row[1].properties.text2.x = Helper.standardTextOffsetx
		if active then
			row[1].handlers.onDropDownConfirmed = menu.dropdownDeploy
		end
		local active = #military > 0
		row[7]:createDropDown(military, {
			height = Helper.standardButtonHeight,
			startOption = "",
			textOverride = ReadText(1001, 8608),
			text2Override = " ",
			bgColor = active and Helper.defaultButtonBackgroundColor or Helper.defaultUnselectableButtonBackgroundColor,
			highlightColor = active and Helper.defaultButtonHighlightColor or Helper.defaultUnselectableButtonHighlightColor
		}):setTextProperties(active and config.activeButtonTextProperties or config.inactiveButtonTextProperties):setText2Properties(active and config.activeButtonTextProperties or config.inactiveButtonTextProperties)	-- Deploy Military
		row[7].properties.text2.halign = "right"
		row[7].properties.text2.x = Helper.standardTextOffsetx
		if active then
			row[7].handlers.onDropDownConfirmed = menu.dropdownDeploy
		end

		local row = table_header:addRow("buttonRow2", { bgColor = Helper.color.transparent, fixed = true })
		local active = (menu.currentplayership ~= 0) and C.HasShipFlightAssist(menu.currentplayership)
		row[1]:createButton(active and { mouseOverText = GetLocalizedKeyName("action", 221) } or config.inactiveButtonProperties):setText(ReadText(1001, 8604), active and config.activeButtonTextProperties or config.inactiveButtonTextProperties)	-- "Flight Assist"
		if active then
			row[1].handlers.onClick = menu.buttonFlightAssist
		end
		row[2]:createButton({ bgColor = menu.dockButtonBGColor, highlightColor = menu.dockButtonHighlightColor }):setText(ReadText(1001, 8605), { halign = "center", color = menu.dockButtonTextColor })	-- "Dock"
		row[2].properties.mouseOverText = GetLocalizedKeyName("action", 175)
		row[2].handlers.onClick = menu.buttonDock
		local active = (menu.currentplayership ~= 0) and C.ToggleAutoPilot(true)
		row[7]:createButton(active and { mouseOverText = GetLocalizedKeyName("action", 179) } or config.inactiveButtonProperties):setText(ReadText(1001, 8603), active and config.activeButtonTextProperties or config.inactiveButtonTextProperties)	-- "Autopilot"
		if active then
			row[7].handlers.onClick = menu.buttonAutoPilot
		end

		if menu.currentplayership ~= 0 then
			local row = table_header:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
			row[1]:createText("", { height = yoffset })

			local row = table_header:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
			row[1]:setColSpan(11):createText(ReadText(1001, 9409), Helper.headerRowCenteredProperties)

			local row = table_header:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
			row[2]:createText(ReadText(1001, 9410), { halign = "center" })
			row[7]:createText(ReadText(1001, 9411), { halign = "center" })

			local weapons = {}
			local numslots = tonumber(C.GetNumUpgradeSlots(menu.currentplayership, "", "weapon"))
			for j = 1, numslots do
				local current = C.GetUpgradeSlotCurrentComponent(menu.currentplayership, "weapon", j)
				if current ~= 0 then
					table.insert(weapons, current)
				end
			end
			local pilot = GetComponentData(menu.currentplayership, "assignedpilot")
			menu.currentammo = {}
			for _, weapon in ipairs(weapons) do
				local numweapongroups = C.GetNumWeaponGroupsByWeapon(menu.currentplayership, weapon)
				local rawweapongroups = ffi.new("UIWeaponGroup[?]", numweapongroups)
				numweapongroups = C.GetWeaponGroupsByWeapon(rawweapongroups, numweapongroups, menu.currentplayership, weapon)
				local uiweapongroups = { primary = {}, secondary = {} }
				for j = 0, numweapongroups-1 do
					if rawweapongroups[j].primary then
						uiweapongroups.primary[rawweapongroups[j].idx] = true
					else
						uiweapongroups.secondary[rawweapongroups[j].idx] = true
					end
				end

				local row = table_header:addRow("weaponconfig", { bgColor = Helper.color.transparent, fixed = true })
				row[1]:setColSpan(2):createText(ffi.string(C.GetComponentName(weapon)))
				row[7]:setColSpan(1)
				for j = 1, 4 do
					row[2 + j]:createCheckBox(uiweapongroups.primary[j], { width = Helper.standardTextHeight, height = Helper.standardTextHeight })
					row[2 + j].handlers.onClick = function() menu.checkboxWeaponGroup(menu.currentplayership, weapon, true, j, not uiweapongroups.primary[j]) end
				end
				for j = 1, 4 do
					row[7 + j]:createCheckBox(uiweapongroups.secondary[j], { width = Helper.standardTextHeight, height = Helper.standardTextHeight })
					row[7 + j].handlers.onClick = function() menu.checkboxWeaponGroup(menu.currentplayership, weapon, false, j, not uiweapongroups.secondary[j]) end
				end

				if C.IsComponentClass(weapon, "missilelauncher") then
					local nummissiletypes = C.GetNumAllMissiles(menu.currentplayership)
					local missilestoragetable = ffi.new("AmmoData[?]", nummissiletypes)
					nummissiletypes = C.GetAllMissiles(missilestoragetable, nummissiletypes, menu.currentplayership)

					local weaponmacro = GetComponentData(ConvertStringTo64Bit(tostring(weapon)), "macro")
					local dropdowndata = {}
					for j = 0, nummissiletypes-1 do
						local ammomacro = ffi.string(missilestoragetable[j].macro)
						if C.IsAmmoMacroCompatible(weaponmacro, ammomacro) then
							table.insert(dropdowndata, {id = ammomacro, text = GetMacroData(ammomacro, "name"), icon = "", displayremoveoption = false})
						end
					end

					-- if the ship has no compatible ammunition in ammo storage, have the dropdown print "Out of ammo" and make it inactive.
					menu.currentammo[tostring(weapon)] = "empty"
					local dropdownactive = true
					if #dropdowndata == 0 then
						dropdownactive = false
						table.insert(dropdowndata, {id = "empty", text = ReadText(1001, 9412), icon = "", displayremoveoption = false})	-- Out of ammo
					else
						-- NB: currentammomacro can be null
						menu.currentammo[tostring(weapon)] = ffi.string(C.GetCurrentAmmoOfWeapon(weapon))
					end

					local row = table_header:addRow("ammo_config", { bgColor = Helper.color.transparent, fixed = true })
					row[1]:createText("    " .. ReadText(1001, 2800) .. ReadText(1001, 120))	-- Ammunition, :
					row[2]:setColSpan(10):createDropDown(dropdowndata, { startOption = function () return menu.getDropDownOption(weapon) end, active = dropdownactive })
					row[2].handlers.onDropDownConfirmed = function(_, newammomacro) C.SetAmmoOfWeapon(weapon, newammomacro) end
				elseif pilot and C.IsComponentClass(weapon, "bomblauncher") then
					local pilot64 = ConvertIDTo64Bit(pilot)
					local numbombtypes = C.GetNumAllInventoryBombs(pilot64)
					local bombstoragetable = ffi.new("AmmoData[?]", numbombtypes)
					numbombtypes = C.GetAllInventoryBombs(bombstoragetable, numbombtypes, pilot64)

					local weaponmacro = GetComponentData(ConvertStringTo64Bit(tostring(weapon)), "macro")
					local dropdowndata = {}
					for j = 0, numbombtypes-1 do
						local ammomacro = ffi.string(bombstoragetable[j].macro)
						if C.IsAmmoMacroCompatible(weaponmacro, ammomacro) then
							table.insert(dropdowndata, { id = ammomacro, text = GetMacroData(ammomacro, "name"), icon = "", displayremoveoption = false })
						end
					end

					-- if the ship has no compatible ammunition in ammo storage, have the dropdown print "Out of ammo" and make it inactive.
					menu.currentammo[tostring(weapon)] = "empty"
					local dropdownactive = true
					if #dropdowndata == 0 then
						dropdownactive = false
						table.insert(dropdowndata, { id = "empty", text = ReadText(1001, 9412), icon = "", displayremoveoption = false })	-- Out of ammo
					else
						-- NB: currentammomacro can be null
						menu.currentammo[tostring(weapon)] = ffi.string(C.GetCurrentAmmoOfWeapon(weapon))
					end

					local row = table_header:addRow("ammo_config", { bgColor = Helper.color.transparent, fixed = true })
					row[1]:createText("    " .. ReadText(1001, 2800) .. ReadText(1001, 120))	-- Ammunition, :
					row[2]:setColSpan(10):createDropDown(dropdowndata, { startOption = function () return menu.getDropDownOption(weapon) end, active = dropdownactive })
					row[2].handlers.onDropDownConfirmed = function(_, newammomacro) C.SetAmmoOfWeapon(weapon, newammomacro) end
				end
			end

			menu.turrets = {}
			local numslots = tonumber(C.GetNumUpgradeSlots(menu.currentplayership, "", "turret"))
			for j = 1, numslots do
				local groupinfo = C.GetUpgradeSlotGroup(menu.currentplayership, "", "turret", j)
				if (ffi.string(groupinfo.path) == "..") and (ffi.string(groupinfo.group) == "") then
					local current = C.GetUpgradeSlotCurrentComponent(menu.currentplayership, "turret", j)
					if current ~= 0 then
						table.insert(menu.turrets, current)
					end
				end
			end

			menu.turretgroups = {}
			local n = C.GetNumUpgradeGroups(menu.currentplayership, "")
			local buf = ffi.new("UpgradeGroup[?]", n)
			n = C.GetUpgradeGroups(buf, n, menu.currentplayership, "")
			for i = 0, n - 1 do
				if (ffi.string(buf[i].path) ~= "..") or (ffi.string(buf[i].group) ~= "") then
					local group = { path = ffi.string(buf[i].path), group = ffi.string(buf[i].group) }
					local groupinfo = C.GetUpgradeGroupInfo(menu.currentplayership, "", group.path, group.group, "turret")
					if (groupinfo.count > 0) then
						group.operational = groupinfo.operational
						group.currentmacro = ffi.string(groupinfo.currentmacro)
						group.slotsize = ffi.string(groupinfo.slotsize)
						table.insert(menu.turretgroups, group)
					end
				end
			end

			if (#menu.turrets > 0) or (#menu.turretgroups > 0) then
				local row = table_header:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
				row[1]:createText("", { height = yoffset })

				local row = table_header:addRow(false, { fixed = true, bgColor = Helper.color.transparent })
				row[1]:setColSpan(11):createText(ReadText(1001, 8612), Helper.headerRowCenteredProperties)

				local turretmodes = {
					[1] = { id = "attackenemies",	text = ReadText(1001, 8614),	icon = "",	displayremoveoption = false },
					[2] = { id = "defend",			text = ReadText(1001, 8613),	icon = "",	displayremoveoption = false },
					[3] = { id = "mining",			text = ReadText(1001, 8616),	icon = "",	displayremoveoption = false },
					[4] = { id = "missiledefence",	text = ReadText(1001, 8615),	icon = "",	displayremoveoption = false },
					[5] = { id = "autoassist",		text = ReadText(1001, 8617),	icon = "",	displayremoveoption = false },
					[6] = { id = "holdfire",		text = ReadText(1041, 10157),	icon = "",	displayremoveoption = false },
				}

				local row = table_header:addRow("turret_config", { bgColor = Helper.color.transparent, fixed = true })
				row[1]:createText(ReadText(1001, 2963))
				row[2]:setColSpan(10):createDropDown(turretmodes, { startOption = function () return menu.getDropDownTurretModeOption(menu.currentplayership, "all") end })
				row[2].handlers.onDropDownConfirmed = function(_, newturretmode) C.SetAllTurretModes(menu.currentplayership, newturretmode) end

				for i, turret in ipairs(menu.turrets) do
					local row = table_header:addRow("turret_config", { bgColor = Helper.color.transparent, fixed = true })
					row[1]:createText(ffi.string(C.GetComponentName(turret)))
					row[2]:setColSpan(10):createDropDown(turretmodes, { startOption = function () return menu.getDropDownTurretModeOption(turret) end })
					row[2].handlers.onDropDownConfirmed = function(_, newturretmode) C.SetWeaponMode(turret, newturretmode) end
				end

				for i, group in ipairs(menu.turretgroups) do
					local row = table_header:addRow("turret_config", { bgColor = Helper.color.transparent, fixed = true })
					row[1]:createText(ReadText(1001, 8023) .. " " .. i .. ((group.currentmacro ~= "") and (" (" .. menu.getSlotSizeText(group.slotsize) .. " " .. GetMacroData(group.currentmacro, "shortname") .. ")") or ""), { color = (group.operational > 0) and Helper.color.white or Helper.color.red })
					row[2]:setColSpan(10):createDropDown(turretmodes, { startOption = function () return menu.getDropDownTurretModeOption(menu.currentplayership, group.path, group.group) end, active = group.operational > 0 })
					row[2].handlers.onDropDownConfirmed = function(_, newturretmode) C.SetTurretGroupMode(menu.currentplayership, group.path, group.group, newturretmode) end
				end
			end
		end
	else
		local row = table_header:addRow("buttonRow1", { bgColor = Helper.color.transparent, fixed = true })
		row[1]:createButton(config.inactiveButtonProperties):setText("", config.inactiveButtonTextProperties)	-- dummy
		if menu.currentplayership ~= 0 then
			row[2]:createButton({ mouseOverText = GetLocalizedKeyName("action", 277) }):setText(ReadText(1002, 20014), { halign = "center" })	-- "Get Up"
			row[2].handlers.onClick = menu.buttonGetUp
		else
			row[2]:createButton({}):setText(ReadText(1001, 7305), { halign = "center" })	-- "Go to Ship"
			row[2].handlers.onClick = menu.buttonGoToShip
		end
		local active = menu.currentplayership ~= 0
		row[7]:createButton(active and { mouseOverText = GetLocalizedKeyName("action", 316) } or config.inactiveButtonProperties):setText(ReadText(1001, 8602), active and config.activeButtonTextProperties or config.inactiveButtonTextProperties)	-- "Ship Information"
		if active then
			row[7].handlers.onClick = menu.buttonDockedShipInfo
		end

		local row = table_header:addRow("buttonRow2", { bgColor = Helper.color.transparent, fixed = true })
		local active = canbuyship
		row[1]:createButton(active and {} or config.inactiveButtonProperties):setText(ReadText(1002, 8008), active and config.activeButtonTextProperties or config.inactiveButtonTextProperties)	-- "Buy Ships"
		if active then
			row[1].handlers.onClick = menu.buttonBuyShip
		end
		local active = cantrade
		row[2]:createButton(active and {} or config.inactiveButtonProperties):setText(ReadText(1002, 9005), active and config.activeButtonTextProperties or config.inactiveButtonTextProperties)	-- "Trade"
		if active then
			row[2].handlers.onClick = menu.buttonTrade
		end
		local active = canmodifyship
		row[7]:createButton(active and {} or config.inactiveButtonProperties):setText(ReadText(1001, 7841), active and config.activeButtonTextProperties or config.inactiveButtonTextProperties)	-- Upgrade / Repair Ship
		if dockedplayerships[1] and (not canequip) then
			row[7].properties.mouseOverText = (C.IsComponentClass(dockedplayerships[1], "ship_l") or C.IsComponentClass(dockedplayerships[1], "ship_xl")) and ReadText(1026, 7807) or ReadText(1026, 7806)
		elseif not isdock then
			row[7].properties.mouseOverText = ReadText(1026, 8014)
		end
		if active then
			row[7].handlers.onClick = menu.buttonModifyShip
		end

		local row = table_header:addRow("buttonRow3", { bgColor = Helper.color.transparent, fixed = true })
		row[1]:createButton(config.inactiveButtonProperties):setText("", config.inactiveButtonTextProperties)	-- dummy
		row[2]:createButton({ mouseOverText = GetLocalizedKeyName("action", 175), bgColor = menu.undockButtonBGColor, highlightColor = menu.undockButtonHighlightColor }):setText(ReadText(1002, 20013), { halign = "center", color = menu.undockButtonTextColor })	-- "Undock"
		row[2].handlers.onClick = menu.buttonUndock
		row[7]:createButton(config.inactiveButtonProperties):setText("", config.inactiveButtonTextProperties)	-- dummy

		local row = table_header:addRow(false, { bgColor = Helper.color.transparent, fixed = true })
		row[1]:setColSpan(11):createBoxText(menu.infoText, { halign = "center", color = Helper.color.warningorange, boxColor = menu.infoBoxColor })
	end


	if menu.table_header then
		table_header:setTopRow(GetTopRow(menu.table_header))
		table_header:setSelectedRow(menu.selectedRows.header or Helper.currentTableRow[menu.table_header])
		table_header:setSelectedCol(menu.selectedCols.header or Helper.currentTableCol[menu.table_header] or 0)
	else
		table_header:setSelectedRow(menu.selectedRows.header)
		table_header:setSelectedCol(menu.selectedCols.header or 0)
	end
	menu.selectedRows.header = nil
	menu.selectedCols.header = nil

	menu.frame.properties.height = table_header:getVisibleHeight() + table_header.properties.y + Helper.scaleY(Helper.standardButtonHeight)

	-- display view/frame
	menu.frame:display()
end

-- handle created frames
function menu.viewCreated(layer, ...)
	menu.table_toplevel, menu.table_topleft, menu.table_header = ...
end

function menu.isDockButtonActive()
	local docktarget = 0
	local softtarget = C.GetSofttarget().softtargetID
	local environmenttarget = C.GetEnvironmentObject()
	if softtarget ~= 0 then
		docktarget = C.GetContextByClass(softtarget, "container", true)
	elseif environmenttarget ~= 0 then
		docktarget = C.GetContextByClass(environmenttarget, "container", true)
	end
	return (menu.currentplayership ~= 0) and (GetComponentData(menu.currentplayership, "isdocking") or ((docktarget ~= 0) and C.RequestDockAt(docktarget, true)))
end

function menu.dockButtonBGColor()
	return menu.isDockButtonActive() and Helper.defaultButtonBackgroundColor or Helper.defaultUnselectableButtonBackgroundColor
end

function menu.dockButtonHighlightColor()
	return menu.isDockButtonActive() and Helper.defaultButtonHighlightColor or Helper.defaultUnselectableButtonHighlightColor
end

function menu.dockButtonTextColor()
	return menu.isDockButtonActive() and Helper.color.white or Helper.color.grey
end

function menu.isUndockButtonActive()
	return (menu.currentplayership ~= 0) and GetComponentData(menu.currentplayership, "isdocked") and (C.UndockPlayerShip(true) == 0) and ((not menu.buildInProgress) or C.CanCancelConstruction(menu.currentcontainer, menu.buildInProgress))
end

function menu.undockButtonBGColor()
	return menu.isUndockButtonActive() and Helper.defaultButtonBackgroundColor or Helper.defaultUnselectableButtonBackgroundColor
end

function menu.undockButtonHighlightColor()
	return menu.isUndockButtonActive() and Helper.defaultButtonHighlightColor or Helper.defaultUnselectableButtonHighlightColor
end

function menu.undockButtonTextColor()
	return menu.isUndockButtonActive() and Helper.color.white or Helper.color.grey
end

function menu.infoText()
	if C.UndockPlayerShip(true) == 1 then
		return ReadText(1001, 8609)
	end
	if menu.currentplayership ~= 0 then
		local constructions = {}
		-- builds in progress
		local n = C.GetNumBuildTasks(menu.currentcontainer, true, true)
		local buf = ffi.new("BuildTaskInfo[?]", n)
		n = C.GetBuildTasks(buf, n, menu.currentcontainer, true, true)
		for i = 0, n - 1 do
			table.insert(constructions, { id = buf[i].id, buildingcontainer = buf[i].buildingcontainer, component = buf[i].component, macro = ffi.string(buf[i].macro), factionid = ffi.string(buf[i].factionid), buildercomponent = buf[i].buildercomponent, price = buf[i].price, ismissingresources = buf[i].ismissingresources, queueposition = buf[i].queueposition, inprogress = true })
		end
		-- other builds
		menu.buildInProgress, menu.buildToCancel = nil, nil
		local n = C.GetNumBuildTasks(menu.currentcontainer, false, true)
		local buf = ffi.new("BuildTaskInfo[?]", n)
		n = C.GetBuildTasks(buf, n, menu.currentcontainer, false, true)
		for i = 0, n - 1 do
			table.insert(constructions, { id = buf[i].id, buildingcontainer = buf[i].buildingcontainer, component = buf[i].component, macro = ffi.string(buf[i].macro), factionid = ffi.string(buf[i].factionid), buildercomponent = buf[i].buildercomponent, price = buf[i].price, ismissingresources = buf[i].ismissingresources, queueposition = buf[i].queueposition, inprogress = false })
		end
		for _, entry in ipairs(constructions) do
			if (entry.component == menu.currentplayership) then
				if entry.inprogress then
					menu.buildInProgress = entry.id
					if C.CanCancelConstruction(menu.currentcontainer, menu.buildInProgress) then
						-- warning but allow undock
						menu.buildToCancel = { entry.id, tonumber(entry.price) }
						return ReadText(1001, 8611)
					else
						-- may not be aborted
						return ReadText(1001, 8610) .. " (" .. ConvertTimeString(C.GetBuildProcessorEstimatedTimeLeft(entry.buildercomponent), "%h:%M:%S") .. ")"
					end
				else
					-- warning but allow undock
					menu.buildToCancel = { entry.id, tonumber(entry.price) }
					return ReadText(1001, 8611)
				end
			end
		end
	end
	return ""
end

function menu.infoBoxColor()
	if C.UndockPlayerShip(true) == 1 then
		return Helper.color.warningorange
	end
	return Helper.color.transparent
end

function menu.getDropDownOption(weapon)
	local currentweapon = ffi.string(C.GetCurrentAmmoOfWeapon(weapon))
	if menu.currentammo[tostring(weapon)] ~= currentweapon then
		menu.currentammo[tostring(weapon)] = currentweapon
	end
	return menu.currentammo[tostring(weapon)]
end

function menu.getDropDownTurretModeOption(defensibleorturret, path, group)
	if (path == nil) and (group == nil) then
		return ffi.string(C.GetWeaponMode(defensibleorturret))
	elseif path == "all" then
		local allmode
		for i, turret in ipairs(menu.turrets) do
			local mode = ffi.string(C.GetWeaponMode(turret))
			if allmode == nil then
				allmode = mode
			elseif allmode ~= mode then
				allmode = ""
				break
			end
		end
		for i, group in ipairs(menu.turretgroups) do
			if group.operational > 0 then
				local mode = ffi.string(C.GetTurretGroupMode(defensibleorturret, group.path, group.group))
				if allmode == nil then
					allmode = mode
				elseif allmode ~= mode then
					allmode = ""
					break
				end
			end
		end
		return allmode
	end
	return ffi.string(C.GetTurretGroupMode(defensibleorturret, path, group))
end

function menu.getSlotSizeText(slotsize)
	if slotsize == "extralarge" then
		return ReadText(1001, 48)
	elseif slotsize == "large" then
		return ReadText(1001, 49)
	elseif slotsize == "medium" then
		return ReadText(1001, 50)
	elseif slotsize == "small" then
		return ReadText(1001, 51)
	end

	return ""
end

function menu.createTopLevel(frame)
	menu.topLevelOffsetY = Helper.createTopLevelTab(menu, menu.mode, frame, "", nil, (menu.mode == "cockpit") or (menu.currentplayership == 0))
end

function menu.onTabScroll(direction)
	if direction == "right" then
		Helper.scrollTopLevel(menu, menu.mode, 1)
	elseif direction == "left" then
		Helper.scrollTopLevel(menu, menu.mode, -1)
	end
end

function menu.onInputModeChanged(_, mode)
	menu.display()
end

-- widget scripts
function menu.buttonGetUp()
	if not C.GetUp() then
		DebugError("failed getting up.")
	end
end

function menu.buttonGoToShip()
	Helper.closeMenuAndOpenNewMenu(menu, "PlatformUndockMenu", { 0, 0 }, true)
	menu.cleanup()
end

function menu.buttonBuyShip()
	local shiptrader, canbuildships = GetComponentData(menu.currentcontainer, "shiptrader", "canbuildships")
	if not shiptrader and not canbuildships then
		DebugError("menu.buttonBuyShip called but " .. ffi.string(C.GetComponentName(menu.currentcontainer)) .. " has no shiptrader or not canbuildships.")
		return
	end
	Helper.closeMenuAndOpenNewMenu(menu, "ShipConfigurationMenu", { 0, 0, menu.currentcontainer, "purchase", {} })
	menu.cleanup()
end

function menu.buttonModifyShip()
	local shiptrader, canequipships = GetComponentData(menu.currentcontainer, "shiptrader", "canequipships")
	if not shiptrader and not canequipships then
		DebugError("menu.buttonModifyShip called but " .. ffi.string(C.GetComponentName(menu.currentcontainer)) .. " has no shiptrader or not canequipships.")
		return
	end
	Helper.closeMenuAndOpenNewMenu(menu, "ShipConfigurationMenu", { 0, 0, menu.currentcontainer, "upgrade", {} })
	menu.cleanup()
end

function menu.buttonTrade()
	-- TODO: re-add ware exchange here, also this is a big simplifications. It could still be that you want to fullfil trade offers of your own stations instead of doing a ware exchange
	--COULD BREAK A LOT OF STUFF
	-- local iswareexchange = false --GetComponentData(menu.currentcontainer, "owner") == "player"
	local iswareexchange = GetComponentData(menu.currentcontainer, "owner") == "player"
	Helper.closeMenuAndOpenNewMenu(menu, "MapMenu", { 0, 0, true, nil, nil, "tradecontext", {menu.currentcontainer, nil, iswareexchange} })
	menu.cleanup()
end

function menu.buttonUndock()
	if menu.isUndockButtonActive() then
		if menu.buildToCancel then
			Helper.closeMenuAndOpenNewMenu(menu, "UserQuestionMenu", { 0, 0, "abortupgrade", { menu.currentcontainer, menu.buildToCancel[1], menu.buildToCancel[2] } }, true)
			menu.cleanup()
		else
			Helper.closeMenu(menu, "close")
			menu.cleanup()
			if not C.UndockPlayerShip(false) then
				DebugError("failed undocking.")
			end
		end
	end
end

function menu.buttonShipInfo()
	Helper.closeMenuAndOpenNewMenu(menu, "MapMenu", { 0, 0, true, nil, nil, "infomode", { "info", menu.currentcontainer } })
	menu.cleanup()
end

function menu.buttonDockedShipInfo()
	Helper.closeMenuAndOpenNewMenu(menu, "MapMenu", { 0, 0, true, nil, nil, "infomode", { "info", menu.currentplayership } })
	menu.cleanup()
end

function menu.buttonAutoPilot()
	C.ToggleAutoPilot(false)
	Helper.closeMenu(menu, "close")
	menu.cleanup()
end

function menu.buttonDock()
	if menu.isDockButtonActive() then
		C.QuickDock()
	end
end

function menu.buttonFlightAssist()
	C.ToggleFlightAssist()
	Helper.closeMenu(menu, "close")
	menu.cleanup()
end

function menu.buttonStopMode()
	C.StopPlayerActivity(GetPlayerActivity())
	menu.display()
end

function menu.dropdownMode(_, id)
	C.StartPlayerActivity(id)
	menu.refresh = getElapsedTime() + 0.11
end

function menu.dropdownDeploy(_, idstring)
	local id, macro = string.match(idstring, "(.+):(.+)")
	for _, entry in ipairs(config.consumables) do
		if entry.id == id then
			entry.callback(menu.currentplayership, macro)
		end
	end
	menu.display()
end

function menu.checkboxWeaponGroup(objectid, weaponid, primary, group, active)
	C.SetWeaponGroup(objectid, weaponid, primary, group, active)
	menu.display()
end

menu.updateInterval = 0.1

-- hook to update the menu while it is being displayed
function menu.onUpdate()
	if menu.refresh then
		if menu.refresh < getElapsedTime() then
			menu.refresh = nil
			menu.display()
			return
		end
	end

	--print("On Update")
	menu.frame:update()
end

-- hook if the highlighted row is changed (is also called on menu creation)
function menu.onRowChanged(row, rowdata)
	--print("Row Changed")
	menu.frame:update()
end

-- hook if the highlighted row is selected
function menu.onSelectElement()
	--print("Element Selected")
end

function menu.close()
	Helper.closeMenu(menu, "close")
	menu.cleanup()
end

-- hook if the menu is being closed
function menu.onCloseElement(dueToClose)
	if (menu.mode == "docked") and (menu.currentplayership ~= 0) then
		if dueToClose == "back" then
			Helper.closeMenuAndOpenNewMenu(menu, "OptionsMenu", nil)
			menu.cleanup()
		end
	else
		Helper.closeMenu(menu, dueToClose)
		menu.cleanup()
	end
end

init()
