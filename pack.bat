del /Q /S .\packing\*
rmdir /Q /S .\packing
for %%I in (.\*.cat .\*.dat) do del /Q "%%I"
"..\..\XRCatTool.exe" -in subst_01 -out subst_01.cat -dump
"..\..\XRCatTool.exe" -in ext_01 -out ext_01.cat -dump
mkdir .\packing\mobileshipproduction
for %%I in (.\*.cat .\*.dat .\content.xml) do copy "%%I" .\packing\mobileshipproduction\
cd .\packing
powershell.exe -nologo -noprofile -command "& { Compress-Archive -Path ./mobileshipproduction -DestinationPath ./mobileshipproduction.zip }"
cd ..\
pause